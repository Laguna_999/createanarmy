package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.tweens.FlxTween;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import flixel.math.FlxRandom;
import flixel.util.FlxTimer;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.util.FlxSpriteUtil;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class PlayState extends FlxState
{
	
	
	public var backgroundSprite : FlxSprite;
	public var overlay : FlxSprite;
	private var ending : Bool;
	

	
	
	private var units0 : FlxTypedGroup<Unit>;
	private var units1 : FlxTypedGroup<Unit>;
	
	private var selectorSprite : FlxSprite;
	private var selectorIndex : Int = 0;
	
	private var enemyTimer : Float = 1.5;
	private var playerTimer : Float = 1.5;
	
	
	private var base0 : Base;
	private var base1 : Base;
	
	private var winText : FlxText;
	
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
		super.create();
		backgroundSprite = new FlxSprite();
		backgroundSprite.makeGraphic(FlxG.width, FlxG.height);
		backgroundSprite.color = Palette.primary3();
		//add(backgroundSprite);
		
		
		units0 = new FlxTypedGroup<Unit>();
		units1 = new FlxTypedGroup<Unit>();
		
		selectorSprite = new FlxSprite();
		selectorSprite.makeGraphic(32, 32);
		selectorSprite.alpha = 0.75;
		
		
		
		ending = false;
		overlay = new FlxSprite();
		overlay.makeGraphic(FlxG.width, FlxG.height, FlxColor.BLACK);
		overlay.alpha = 0;
		add(overlay);
	
		
	
		
	
		//spawnEnemyUnit();
		base0 = new Base(0);
		base1 = new Base(1);
		
		winText = new FlxText(0, 0, 0, "You Loose", 16);
		winText.alpha = 0;
		winText.screenCenter();
		winText.y = 45;
		winText.alignment = "CENTER";
		
	}
	
	
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	
	override public function draw() : Void
	{
		super.draw();
		
		backgroundSprite.draw();
		
		base0.draw();
		base1.draw();
		
		units1.draw();
		units0.draw();
		
		selectorSprite.draw();
		
		base0.drawhud();
		base1.drawhud();
		
		overlay.draw();
		
		winText.draw();
	}
	
	/**
	 * Function that is called once every frame.
	 */
	override public function update(elapsed : Float):Void
	{
		super.update(elapsed);
		
		clearUnits();
		
		overlay.update(elapsed);
	
		
		
		base0.update(elapsed);
		base1.update(elapsed);
		
		if (!ending)
		{
			//trace(playerTimer);
			units0.update(elapsed);
			units1.update(elapsed);
			
			if (FlxG.keys.justPressed.DOWN)
			{
				selectorIndex++;
				if (selectorIndex >= 6)
					selectorIndex = 0;
			}
			if (FlxG.keys.justPressed.UP)
			{
				selectorIndex--;
				if (selectorIndex < 0)
					selectorIndex = 5;
			}
			
			placeSelector();
			
			playerTimer -= elapsed;
			
			if (FlxG.keys.justPressed.SPACE &&  playerTimer <= 0)
			{
				playerTimer = 1.3;
				SpawnUnit();
			}
			
			
			
			enemyTimer -= elapsed;
			if (enemyTimer <= 0)
			{
				enemyTimer = 1.5;
				spawnEnemyUnit();
			}
			
			
		
			
			var speed : Float = 108; 
		
			
			FlxG.collide(units0, units1, doCollide);
			
			FlxG.overlap(units0, base1, doCollideBase);
			FlxG.overlap(units1, base0, doCollideBase);
		}
		
		
		
	}	
	
	
	public function doCollideBase (o1, o2)
	{
		if (Std.is(o1, Unit)&& Std.is(o2, Base))
		{
			trace("despawn");
			var s1 : Unit = cast o1;
			var b : Base = cast o2;
			s1.alive = false;
			b.lives -= 1;
			
			CheckEnd();
		}
	}
	
	function CheckEnd() 
	{
		if (base0.lives == 0)
		{
			EndGame();
			winText.text = "You Loose";
			winText.alpha = 1;
		}
		else if (base1.lives == 0)
		{
			EndGame();
			winText.text = "You Win";
			winText.alpha = 1;
		}
	}
	
	function clearUnits() 
	{
		{
			var n : FlxTypedGroup<Unit> = new  FlxTypedGroup<Unit> ();
			units0.forEach(function (u) { if (u.alive) n.add(u); } );
			units0 = n;
		}
		{
			var n : FlxTypedGroup<Unit> = new  FlxTypedGroup<Unit> ();
			units1.forEach(function (u) { if (u.alive) n.add(u); } );
			units1 = n;
		}
	}
	
	public function doCollide (o1, o2)
	{
		trace("docollide");
		if (Std.is(o1, Unit)&& Std.is(o2, Unit))
		{
			
			var s1 : Unit = cast o1;
			var s2 : Unit = cast o2;
			if (s2.attackTimer <= 0)
			{
				s1.takeDamage(s2.damage);
				s2.attackTimer = 0.25;
			}
			if (s1.attackTimer <= 0)
			{
				s2.takeDamage(s1.damage);
				s1.attackTimer = 0.25;
			}
		}
	
		
	}
	
	function placeSelector() 
	{
		selectorSprite.setPosition(32, selectorIndex * 48 + 128);
	}
	
	function spawnEnemyUnit() 
	{
		var u : Unit = new Unit(FlxG.random.int(0,5), 1);
		units1.add(u);
	}
	
	function SpawnUnit() 
	{
		trace("spawn");
		var u : Unit = new Unit(selectorIndex, 0);
		units0.add(u);
	}
	

	
	function EndGame() 
	{
		if (!ending)
		{
			ending = true;
	
			
			FlxTween.tween(overlay, {alpha : 1.0}, 0.9);
			
			var t: FlxTimer = new FlxTimer();
			t.start(1,function(t:FlxTimer): Void { FlxG.switchState(new MenuState()); } );
		}
		
	}
	
}