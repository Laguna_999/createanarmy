package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.text.FlxText;

/**
 * ...
 * @author 
 */
class Base extends FlxSprite
{
	public var _fraction : Int;
	
	public var lives : Int = 3;
	
	private var livetext : FlxText;

	public function new(fraction : Int) 
	{
		super();
		
		_fraction = fraction;
		
		if (_fraction == 0)
		{
			this.makeGraphic(32, FlxG.height, Palette.primary2());
			this.x = 16;
		}
		else
		{
			this.makeGraphic(32, FlxG.height, Palette.secondary2());
			this.x = FlxG.width - 16 - this.width;
		}
		livetext = new FlxText(this.x, this.y + 64, 0, "3", 16);
		
		
	}
	
	public override function update(elapesd : Float)
	{
		super.update(elapesd);
		
		livetext.text = Std.string(lives);
	}
	
	
	public function drawhud()
	{
		livetext.draw();
	}
}