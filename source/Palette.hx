package;
import flixel.math.FlxRandom;
import flixel.util.FlxColor;

/**
 * ...
 * @author 
 */
class Palette
{
	
	private static var angle : Float = 300;
	
	public static function changeColor()
	{
		angle += 150;
		if (angle >= 360) angle -= 360;
	}
	public static function reset()
	{
		angle = 150;
	}
	
	public static function primary0 () : Int 
	{
		//return FlxColor.fromRGB(102, 51, 102,255);
		return FlxColor.fromHSB(angle, 0.5, 0.4);
	}
	
	public static function primary1 () : Int 
	{
		//return FlxColor.fromRGB(194, 170,194,255);
		return FlxColor.fromHSB(angle, 0.124, 0.761);
	}
	
	public static function primary2 () : Int 
	{
		//return FlxColor.fromRGB(136, 91, 136,255);
		return FlxColor.fromHSB(angle, 0.331, 0.533);
	}
	
	public static function primary3 () : Int 
	{
		//return FlxColor.fromRGB(75, 23, 75,255);
		return FlxColor.fromHSB(angle, 0.685, 0.286);
	}
	
	public static function primary4 () : Int 
	{
		//return FlxColor.fromRGB(38,1,38,255);
		return FlxColor.fromHSB(angle, 0.974, 0.149);
	}
	
	
	public static function secondary0 () : Int 
	{
		//return FlxColor.fromRGB(102, 51, 102,255);
		return FlxColor.fromHSB(angle+180, 0.5, 0.4);
	}
	
	public static function secondary1 () : Int 
	{
		//return FlxColor.fromRGB(194, 170,194,255);
		return FlxColor.fromHSB(angle+180, 0.124, 0.761);
	}
	
	public static function secondary2 () : Int 
	{
		//return FlxColor.fromRGB(136, 91, 136,255);
		return FlxColor.fromHSB(angle+180, 0.331, 0.533);
	}
	
	public static function secondary3 () : Int 
	{
		//return FlxColor.fromRGB(75, 23, 75,255);
		return FlxColor.fromHSB(angle+180, 0.685, 0.286);
	}
	
	public static function secondary4 () : Int 
	{
		//return FlxColor.fromRGB(38,1,38,255);
		return FlxColor.fromHSB(angle+180, 0.974, 0.149);
	}
	
}