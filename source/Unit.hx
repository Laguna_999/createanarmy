package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;

/**
 * ...
 * @author 
 */
class Unit extends FlxSprite
{

	static private var vel : Float = 80;
	
	public var attackTimer :Float = 0;

	
	public var damage : Float;
	
	private var _fraction : Int;
	
	public function new(lane : Int, fraction : Int) 
	{
		super();
		_fraction = fraction;
		
		this.damage = FlxG.random.float(0.15, 0.34);
		
		//this.immovable = true;
		if (fraction == 0)
		{
			
			this.makeGraphic(32, 32, Palette.primary0());
			this.x = 48;
			this.velocity.x = vel;
		}
		else
		{
			
			this.makeGraphic(32, 32, Palette.secondary0());
			this.x = FlxG.width - 48 - this.width;
			this.velocity.x = -vel;
		}
		
		this.y = 128 + 48 * lane;
	}
	
	public override function update(elapsed : Float)
	{
		super.update(elapsed);
		
		trace(x + " " + y);
		
		attackTimer -= elapsed;
		//trace(attackTimer);
		
			if (_fraction == 0)
		{
			this.velocity.x = vel;
		}
		else
		{
			this.velocity.x = -vel;
		}
		
		
	}
	
	public function takeDamage (a : Float)
	{
		health -= a;
		FlxTween.color(this, 0.2, FlxColor.RED, FlxColor.WHITE);
		if (health <= 0)
		{
			trace("die");
			alive = false;
		}
	}
	
}